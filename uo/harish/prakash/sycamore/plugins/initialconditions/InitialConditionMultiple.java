/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.initialconditions;

import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.engine.SycamoreRobotMatrix;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditions;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditionsImpl;
import it.diunipi.volpi.sycamore.util.ApplicationProperties;
import it.diunipi.volpi.sycamore.util.PropertyManager;
import it.diunipi.volpi.sycamore.util.SycamoreUtil;
import net.xeoh.plugins.base.annotations.PluginImplementation;

/**
 * @author harry
 *
 */
@PluginImplementation
public class InitialConditionMultiple extends InitialConditionsImpl<Point2D> {

	public static String	_author				= "Harish Prakash";
	public static TYPE		_type				= TYPE.TYPE_2D;
	public static String	_shortDescription	= "Multiple Conditions";
	public static String	_pluginName			= "MultipleConditions";
	public static String	_description		= "Use the tools provided to select multiple initial conditions";

	// Generic Inherited properties
	@Override
	public TYPE getType() {

		return _type;
	}

	@Override
	public String getAuthor() {

		return _author;
	}

	@Override
	public String getPluginShortDescription() {

		return _shortDescription;
	}

	@Override
	public String getPluginLongDescription() {

		return _description;
	}

	@Override
	public SycamorePanel getPanel_settings() {

		return InitialConditionMultipleConfigurationPanel.getUniqueInstance();
	}

	@Override
	public String getPluginName() {

		return _pluginName;
	}
	// END OF Generic Inherited Properties

	@Override
	public Point2D nextStartingPoint(SycamoreRobotMatrix<Point2D> robots) {

		InitialConditions<Point2D> condition;
		condition = InitialConditionMultipleConfigurationPanel.getUniqueInstance().getInitialConditionPluginAt(robots.robotsCount());
		if (condition == null || condition.getClass() == getClass()) {

			float minX = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MIN_X);
			float maxX = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MAX_X);
			float minY = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MIN_Y);
			float maxY = PropertyManager.getSharedInstance().getIntegerProperty(ApplicationProperties.INITIAL_POSITION_MAX_Y);

			return SycamoreUtil.getRandomPoint2D(minX, maxX, minY, maxY);
		}
		else {
			return condition.nextStartingPoint(robots);
		}
	}
}
