/**
 * 
 */
package uo.harish.prakash.sycamore.plugins.initialconditions;

import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;
import it.diunipi.volpi.sycamore.engine.Point2D;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine;
import it.diunipi.volpi.sycamore.engine.SycamoreEngine.TYPE;
import it.diunipi.volpi.sycamore.gui.SycamorePanel;
import it.diunipi.volpi.sycamore.plugins.initialconditions.InitialConditions;
import it.diunipi.volpi.sycamore.util.ApplicationProperties;
import net.xeoh.plugins.base.PluginManager;
import net.xeoh.plugins.base.impl.PluginManagerFactory;
import net.xeoh.plugins.base.util.PluginManagerUtil;

/**
 * @author harry
 *
 */
public class InitialConditionMultipleConfigurationPanel extends SycamorePanel {

	/**
	 * 
	 */
	private static final long									serialVersionUID	= 1L;
	private static InitialConditionMultipleConfigurationPanel	uniqueInstance;

	private JPanel contentPanel;

	private JLabel	labelInitialCondition;
	private JLabel	labelNumberOfRobots;

	private JComboBox<InitialConditions<?>> comboInitialCondition;

	private JTextField textNumberOfRobots;

	private JButton	buttonAdd;
	private JButton	buttonClear;

	private JScrollPane			scrollPreview;
	private JTable				tablePreview;
	private DefaultTableModel	modelPreview;

	private ArrayList<SelectionData>	listSelectionData;
	private Integer						countRegisteredRobots;

	@SuppressWarnings("rawtypes")
	@Override
	public void setAppEngine(SycamoreEngine appEngine) {}

	@Override
	public void updateGui() {}

	@Override
	public void reset() {}

	private InitialConditionMultipleConfigurationPanel() {
		countRegisteredRobots = 0;
		setupGUI();
	}

	private void setupGUI() {

		BoxLayout layout = new BoxLayout(this, BoxLayout.Y_AXIS);
		setLayout(layout);

		GridBagLayout contentLayout = new GridBagLayout();
		contentLayout.rowWeights = new double[] { Double.MIN_VALUE, Double.MIN_VALUE, Double.MIN_VALUE, 1 };
		contentLayout.columnWeights = new double[] { Double.MIN_VALUE, Double.MIN_VALUE, Double.MIN_VALUE, 1 };
		getContentPanel().setLayout(contentLayout);

		GridBagConstraints c;
		Insets inset = new Insets(5, 5, 5, 5);

		// Labels
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 0;
		c.insets = inset;
		c.fill = GridBagConstraints.BOTH;
		getContentPanel().add(getLabelInitialCondition(), c);

		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 1;
		c.insets = inset;
		c.fill = GridBagConstraints.BOTH;
		getContentPanel().add(getLabelNumberOfRobots(), c);
		// END of Labels

		// ComboBoxes
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 0;
		c.gridwidth = 3;
		c.insets = inset;
		c.fill = GridBagConstraints.BOTH;
		getContentPanel().add(getComboInitialCondition(), c);
		// END of ComboBoxes

		// TextFields
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 1;
		c.gridwidth = 3;
		c.insets = inset;
		c.fill = GridBagConstraints.BOTH;
		getContentPanel().add(getTextNumberOfRobots(), c);
		// END of TextFields

		// Buttons
		c = new GridBagConstraints();
		c.gridx = 1;
		c.gridy = 2;
		c.insets = inset;
		c.fill = GridBagConstraints.BOTH;
		getContentPanel().add(getButtonAdd(), c);

		c = new GridBagConstraints();
		c.gridx = 2;
		c.gridy = 2;
		c.insets = inset;
		c.fill = GridBagConstraints.BOTH;
		getContentPanel().add(getButtonClear(), c);
		// END of Buttons

		// Preview
		c = new GridBagConstraints();
		c.gridx = 0;
		c.gridy = 3;
		c.gridwidth = 4;
		c.insets = inset;
		c.fill = GridBagConstraints.BOTH;
		getContentPanel().add(getScrollPreview(), c);
		// END of Preview

		Dimension size = new Dimension(500, 250);
		getContentPanel().setPreferredSize(size);
		getContentPanel().setMinimumSize(size);
		add(getContentPanel());
	}

	/**
	 * @return the contentPanel
	 */
	private JPanel getContentPanel() {

		if (contentPanel == null) {
			contentPanel = new JPanel();
		}
		return contentPanel;
	}

	/**
	 * @return the labelInitialCondition
	 */
	private JLabel getLabelInitialCondition() {

		if (labelInitialCondition == null) {
			labelInitialCondition = new JLabel("Condition");
		}
		return labelInitialCondition;
	}

	/**
	 * @return the labelNumberOfRobots
	 */
	private JLabel getLabelNumberOfRobots() {

		if (labelNumberOfRobots == null) {
			labelNumberOfRobots = new JLabel("Number of Robots");
		}
		return labelNumberOfRobots;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private InitialConditions<Point2D>[] getInitialConditionPlugins() {

		PluginManager pluginManager = PluginManagerFactory.createPluginManager();
		pluginManager.addPluginsFrom(new File(ApplicationProperties.WORKSPACE_DIR.getDefaultValue(), "Plugins").toURI());
		PluginManagerUtil pluginManagerUtil = new PluginManagerUtil(pluginManager);
		java.util.Collection<InitialConditions> availableConditions = pluginManagerUtil.getPlugins(InitialConditions.class);

		ArrayList<InitialConditions<Point2D>> conditions = new ArrayList<>();

		for (InitialConditions condition : availableConditions) {
			if (condition.getPluginName().equals(InitialConditionMultiple._pluginName)) {
				continue;
			}
			if (condition.getType() != TYPE.TYPE_2D) {
				continue;
			}
			conditions.add(condition);
		}

		Collections.sort(conditions, new Comparator<InitialConditions<Point2D>>() {

			@Override
			public int compare(InitialConditions<Point2D> o1, InitialConditions<Point2D> o2) {

				if (o1 == null && o2 == null) {
					return 0;
				}
				else if (o1 == null) {
					return -1;
				}
				else if (o2 == null) {
					return 1;
				}
				return o1.getPluginName().compareTo(o2.getPluginName());
			}
		});

		InitialConditions<Point2D>[] normalized = new InitialConditions[conditions.size()];
		normalized = conditions.toArray(normalized);
		return normalized;
	}

	/**
	 * @return the comboInitialCondition
	 */
	private JComboBox<InitialConditions<?>> getComboInitialCondition() {

		if (comboInitialCondition == null) {
			comboInitialCondition = new JComboBox<>(getInitialConditionPlugins());
		}
		return comboInitialCondition;
	}

	/**
	 * @return the textNumberOfRobots
	 */
	private JTextField getTextNumberOfRobots() {

		if (textNumberOfRobots == null) {
			textNumberOfRobots = new JTextField();
		}
		return textNumberOfRobots;
	}

	/**
	 * @return the buttonAdd
	 */
	private JButton getButtonAdd() {

		if (buttonAdd == null) {
			buttonAdd = new JButton("Add");
			buttonAdd.addActionListener(new ActionListener() {

				@SuppressWarnings("unchecked")
				@Override
				public void actionPerformed(ActionEvent e) {

					// Add selection
					Integer numberOfRobots = null;
					InitialConditions<Point2D> initialCondition = (InitialConditions<Point2D>) getComboInitialCondition().getSelectedItem();

					if (initialCondition == null) {
						return;
					}

					// @formatter:off
					try { numberOfRobots = Integer.parseInt(getTextNumberOfRobots().getText()); }
					catch (NumberFormatException ex){}
					// @formatter:on

					addSelection(initialCondition, numberOfRobots);
					// END of Add selection

					// Clear Button
					getButtonClear().setEnabled(true);
					// END of Clear Button

				}
			});
		}
		return buttonAdd;
	}

	/**
	 * @return the buttonClear
	 */
	private JButton getButtonClear() {

		if (buttonClear == null) {
			buttonClear = new JButton("Clear");
			buttonClear.setEnabled(false);
			buttonClear.addActionListener(new ActionListener() {

				@Override
				public void actionPerformed(ActionEvent e) {

					// Remove Items
					removeSelections();
					// END of Remove Items

					// Buttons
					getButtonAdd().setEnabled(true);
					((JButton) e.getSource()).setEnabled(false);
					// END of Buttons
				}
			});
		}
		return buttonClear;
	}

	/**
	 * @return the scrollPreview
	 */
	private JScrollPane getScrollPreview() {

		if (scrollPreview == null) {
			scrollPreview = new JScrollPane(getTablePreview());
		}
		return scrollPreview;
	}

	/**
	 * @return the tablePreview
	 */
	private JTable getTablePreview() {

		if (tablePreview == null) {

			final SycamorePanel parent = this;
			tablePreview = new JTable(getModelPreview());
			tablePreview.addMouseListener(new MouseAdapter() {

				@Override
				public void mouseClicked(MouseEvent e) {

					if (e.getClickCount() == 2 && listSelectionData != null) {
						SelectionData data = listSelectionData.get(tablePreview.rowAtPoint(e.getPoint()));
						if (data.getInitialCondition() != null && data.getInitialCondition().getPanel_settings() != null) {
							JOptionPane.showOptionDialog(
								parent,
								data.getInitialCondition().getPanel_settings(),
								"Config",
								JOptionPane.DEFAULT_OPTION,
								JOptionPane.PLAIN_MESSAGE,
								null,
								new String[] { "OK" },
								"OK");
						}
					}
				}
			});

			tablePreview.addKeyListener(new KeyAdapter() {

				@Override
				public void keyPressed(KeyEvent e) {

					if (e.getKeyCode() == KeyEvent.VK_ENTER) {
						e.consume();
						SelectionData data = listSelectionData.get(tablePreview.getSelectedRow());
						if (data.getInitialCondition() != null && data.getInitialCondition().getPanel_settings() != null) {
							JOptionPane.showOptionDialog(
								parent,
								data.getInitialCondition().getPanel_settings(),
								"Config",
								JOptionPane.DEFAULT_OPTION,
								JOptionPane.PLAIN_MESSAGE,
								null,
								new String[] { "OK" },
								"OK");
						}
					}
				}
			});
		}
		return tablePreview;
	}

	/**
	 * @return the modelPreview
	 */
	private DefaultTableModel getModelPreview() {

		if (modelPreview == null) {
			final String[] columns = new String[] { "Initial Condition", "Robots" };
			modelPreview = new DefaultTableModel() {

				/**
				 * 
				 */
				private static final long serialVersionUID = 1L;

				@Override
				public String getColumnName(int column) {

					return columns[column];
				}

				@Override
				public int getColumnCount() {

					return columns.length;
				}

				@Override
				public boolean isCellEditable(int row, int column) {

					return false;
				}
			};
		}
		return modelPreview;
	}

	/**
	 * @return the uniqueInstance
	 */
	public static InitialConditionMultipleConfigurationPanel getUniqueInstance() {

		if (uniqueInstance == null) {
			uniqueInstance = new InitialConditionMultipleConfigurationPanel();
		}
		return uniqueInstance;
	}

	public InitialConditions<Point2D> getInitialConditionPluginAt(int number) {

		if (listSelectionData == null) {
			return null;
		}
		else if (countRegisteredRobots != null) {
			number = number % countRegisteredRobots;
		}

		int runningCounter = 0;

		for (SelectionData data : listSelectionData) {
			Integer associatedNumber = data.getNumberOfRobots();
			InitialConditions<Point2D> initialCondition = data.getInitialCondition();

			if (associatedNumber == null) {
				return initialCondition;
			}
			else if (number < runningCounter + associatedNumber) {
				return initialCondition;
			}
			else {
				runningCounter += associatedNumber;
			}
		}

		return null;
	}

	private class SelectionData {

		private Integer						numberOfRobots;
		private InitialConditions<Point2D>	initialCondition;

		public SelectionData(InitialConditions<Point2D> initialCondition, Integer numberOfRobots) {
			this.numberOfRobots = numberOfRobots;
			this.initialCondition = initialCondition;
		}

		/**
		 * @return the numberOfRobots
		 */
		public Integer getNumberOfRobots() {

			return numberOfRobots;
		}

		/**
		 * @return the initialCondition
		 */
		public InitialConditions<Point2D> getInitialCondition() {

			return initialCondition;
		}
	}

	/**
	 * @return the listSelectionData
	 */
	private void addSelection(InitialConditions<Point2D> initialCondition, Integer numberOfRobots) {

		if (initialCondition == null) {
			return;
		}

		if (countRegisteredRobots == null) {
			return;
		}

		else if (numberOfRobots == null) {
			countRegisteredRobots = null;
			getButtonAdd().setEnabled(false);
		}

		else if (numberOfRobots > 0) {
			countRegisteredRobots += numberOfRobots;
		}

		else {
			return;
		}

		if (listSelectionData == null) {
			listSelectionData = new ArrayList<>();
		}

		SelectionData data = new SelectionData(initialCondition, numberOfRobots);

		// Add to the list
		listSelectionData.add(data);
		// END of Add to the list

		// Add to the table
		getModelPreview().addRow(
			new Object[] { initialCondition.getPluginName(), (numberOfRobots == null ? "Infinite" : numberOfRobots.toString()) });
		// END of Add to the table
	}

	private void removeSelections() {

		countRegisteredRobots = 0;
		listSelectionData = null;

		int rowCount = getModelPreview().getRowCount();
		while (--rowCount > -1) {
			getModelPreview().removeRow(0);
		}
	}
}
